#etapa 0, incializando herramientas de despliegue e instalacion
FROM  node:12

WORKDIR /app
COPY ./dist/sif-application /app
RUN echo $(ls /)
#RUN npm install
#RUN npm run build -- --prod

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:alpine
RUN echo $(ls /)
COPY --from=0 /app /usr/share/nginx/html
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf

#docker build . -t sif
#docker run -d -p 80:80 --name angularFul   sif