import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
 // Componentes
import { LoginComponent } from './components/login/login.component';

import { RouteGuardService } from './services/util/route-guard.service';
import { WelcomeComponent } from './components/welcome/welcome.component';

const routes: Routes = [
  { path: 'ingreso', component: LoginComponent},
  { path: 'welcome', component: WelcomeComponent, canActivate: [RouteGuardService]},
  { path: '', component: WelcomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
