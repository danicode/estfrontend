import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../authentication/authentication.service';
import { HttpClient } from '@angular/common/http';
import { API_URL } from './../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class RouteGuardService implements CanActivate {
  activate = false;
  constructor(
    private authenticate: AuthenticationService,
    private router: Router,
    private http: HttpClient
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    //utilizar los roles para validar los paths, urgente , crear un metodo en Authentication service para saber si tiene algun rol
    //por el momento solo se esta ocultando los enlaces, pero no protegiendo las interfaces
    if (this.authenticate.isAuthenticated()) {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }
}
