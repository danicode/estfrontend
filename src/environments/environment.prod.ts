export const API_URL = 'http://200.7.161.73';
export const TOKEN = 'token';
export const AUTHENTICATED_USER = 'usuarioAutenticado';
export const ROLES = {
  ROLE_ADMIN : 'ROLE_ADMIN',
  ROLE_ESTUDIANTE : 'ROLE_AUTORIDAD_1',
  ROLE_AUTORIDAD_2 : 'ROLE_AUTORIDAD_2'};
export const environment = {
  production: true
};
